class ControllerA{

    constructor(services){
        // base d'un controller => 
        this.services = services
        this.templateName = "/views/pages/pageA.json"
        this.template = null
        this.view = new View()

        // dépdent à se controller
        this.title = "Page A"
        this.list = [
            {id: 1, text: "YO"}
        ]

    }

    getTemplateName(){
        return this.templateName
    }

    openController(root){
        fileManager.readTextFile(this.getTemplateName(), (data) => {

            this.component = this.view.createComponent(data, this)
            console.log(this.component)
            root.appendChild(this.component["componentParent"])

            this.actionBinder(this.component["buttons"])
            this.inputBinder(this.component["inputs"])

            var todosList = this.services["todosServices"].getTodosList()
            for(var t = 0; t < todosList.length; t ++){
                // console.log(todosList[t])
                this.addTodoItem(todosList[t])
            }

        })
       
    }

    actionBinder(buttons){
        console.log(buttons)
        this.buttons = this.buttons != null ? this.buttons : {}
        for(var b = 0; b < buttons.length; b ++){
            var button = buttons[b]
            this.buttons[button["buttonData"]["bind-click"]]
            button["elementRef"].addEventListener(button["buttonData"].hasOwnProperty("bind-event") ? button["buttonData"]["bind-event"] : 'click', (event) => { this.eventHandler(event) })
        }
    }

    inputBinder(inputs){
        this.inputs = this.inputs != null ? this.inputs : {}
        for(var i = 0; i < inputs.length; i++){
            this.inputs[inputs[i]["inputData"]["bind-input"]] = inputs[i]["elementRef"]
        }
        console.log("inputs : ", this.inputs)
    }

    eventHandler(event){
        switch(event.target.id){
            case "addTodoItem":
                this.addTodoItem(this.services["todosServices"].addTodo(this.inputs["result"].value))
                this.inputs["result"].value = ""
                break;
            case "removeTodoItem":
                this.removeTodoItem(event.target)
                break;
            case "toggleChange":
                var list = document.getElementById("ma-liste")
                this.services["todosServices"].toggleChange(Array.prototype.indexOf.call(list.children, event.target.parentElement.parentElement), event.target.checked)
                break;
            default:
                break;
        }
    }

    addTodoItem(todoItem = null){
        var list = document.getElementById("ma-liste")
        fileManager.readTextFile("/views/components/item.json", (data) => {
            var compo = this.view.createComponent(data, todoItem)
            list.appendChild(compo["componentParent"])
            this.actionBinder(compo["buttons"])
            this.inputBinder(compo["inputs"])
            compo["componentParent"].querySelector("#toggleChange").checked = todoItem["toggle"]

        })

    }

    removeTodoItem(element){
        var list = document.getElementById("ma-liste")
        var index = Array.prototype.indexOf.call(list.children, element.parentElement.parentElement)
        this.services["todosServices"].removeTodo(index)
        list.removeChild(element.parentNode.parentNode)
    }
}
