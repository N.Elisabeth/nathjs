class ControllerB{

    constructor(){
        this.templateName = "/views/pages/pageB.json"
        this.view = new View()
    }

    getTemplateName(){
        return this.templateName
    }

    openController(root){
        fileManager.readTextFile(this.getTemplateName(), (data) => {
            this.component = this.view.createComponent(data, this)
            root.appendChild(this.component["componentParent"])
        })
       
    }
}