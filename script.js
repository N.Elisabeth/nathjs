class Application {
    
    constructor(router){
        this.router = router

        this.app = document.getElementById('root')
        
        this.controllerList = {
            "ControllerA": new ControllerA({"todosServices": todosService}),
            "ControllerB": new ControllerB()
        }
        
        this.router.setControllersList(this.controllerList)
        this.currentController = this.router.chooseController(window.location.hash)
        this.currentController.openController(this.app)

        window.addEventListener('popstate', (event) => {
            this.currentController = this.router.chooseController(window.location.hash)
            this.clearPage()
            this.currentController.openController(this.app)
        });

    }

    clearPage(){
        while(this.app.firstChild != null){
            this.app.removeChild(this.app.firstChild)
        }
    }
}
// const app = new Application(new Router());


// console.log($("#root"))

// var data = [
//     {
//       "name": "Robert",
//       "nickname": "Bob",
//       "showNickname": true
//     },
//     {
//       "name": "Susan",
//       "nickname": "Sue",
//       "showNickname": false
//     }
// ];
  
// var template = $.templates("#theTmpl");

// var htmlOutput = template.render(data);
// $("#root").html(htmlOutput);


// var db = openDatabase('nathJsDb', '1.0', 'my first database', 2 * 1024 * 1024)
// db.transaction(function (tx) {
//     tx.executeSql('CREATE TABLE foo (id unique, text)');
// });


let openRequest = indexedDB.open("nathJsDb", 1)

openRequest.onupgradeneeded = function(){
    db = openRequest.result

    const store = db.createObjectStore("books", {keyPath: "isbn"});
    const titleIndex = store.createIndex("by_title", "title", {unique: true});
    const authorIndex = store.createIndex("by_author", "author");

    store.put({title: "Quarry Memories", author: "Fred", isbn: 123456});
    store.put({title: "Water Buffaloes", author: "Fred", isbn: 234567});
    store.put({title: "Bedrock Nights", author: "Barney", isbn: 345678});
}
openRequest.onerror = function(){
    alert("Impossible de se connecter")
}
openRequest.onsuccess = function(){
    db = openRequest.result

    const tx = db.transaction("books", "readonly");
    const store = tx.objectStore("books");
    const index = store.index("by_title");

    db.onerror = function(e){
        console.log("ERROR" + e.target.errorCode)
    }

    let var1 = index.get("Quarry Memories")
    var1.onsuccess = function(){
        console.log(var1.result)
    }
}