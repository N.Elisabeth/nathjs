class FileManager{

    constructor(){
        this.currentText = ""
    }
    
    readTextFile(file, callback) {
        var rawFile = new XMLHttpRequest();
        rawFile.overrideMimeType("application/json");

        this.currentText = ""

        rawFile.open("GET", file, true);
        rawFile.onreadystatechange = function () {
            if (rawFile.readyState === 4) {
                callback(JSON.parse(rawFile.responseText))
            }
        }
        rawFile.send();
    }
    
}

const fileManager = new FileManager();