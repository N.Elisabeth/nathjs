class View{

    constructor(){}


    createElement(elementJson, controller){
        const element = document.createElement(elementJson["tag"])
        if(elementJson.hasOwnProperty("content")) element.textContent = elementJson["content"]
        if(elementJson["tag"] == "input" &&  elementJson.hasOwnProperty("input-type")) element.type =  elementJson["input-type"]
        if(elementJson.hasOwnProperty("bind-content")) element.textContent = controller[elementJson["bind-content"]]
        if(elementJson.hasOwnProperty("class")) element.classList.add(elementJson["class"])
        if(elementJson.hasOwnProperty("id")) element.id = elementJson["id"]
        return element
    }

    createComponent(template, controller){

        var componentDiv = template["tag"] ? document.createElement(template["tag"]) : document.createElement("div")
        var buttons = []
        var inputs = []

        for(var c = 0; c < template.childs.length; c++){
            var child = template.childs[c];

            if(child.hasOwnProperty("childs")){

                var childComponent = this.createComponent(child, controller)
                componentDiv.appendChild(childComponent["componentParent"]);

                buttons = [...buttons,...childComponent["buttons"]];
                inputs = [...inputs,...childComponent["inputs"]];

            }else{
                var childElement = this.createElement(child, controller)
            
                componentDiv.appendChild(childElement);
    
                if(child.hasOwnProperty("bind-click")){
                    childElement.id = child["bind-click"]
                    buttons.push({
                        "elementRef": childElement,
                        "buttonData": child
                    })
                }
                else if(child.hasOwnProperty("bind-input")){
                    childElement.id = child["bind-input"]
                    inputs.push({
                        "elementRef": childElement,
                        "inputData": child
                    })
                }
            }            
        }

        if(template.hasOwnProperty("class")) componentDiv.classList.add(template["class"])

        return {
            "componentParent" : componentDiv,
            "buttons": buttons,
            "inputs": inputs,
        }
    }

    bindAction(element, className, handler){
        element.addEventListener('click', event => {
            if(event.target.className = className){
                handler(event.target)
            }
        })
    }
}