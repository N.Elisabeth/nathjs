class Controller {
    constructor(controllerName = "baseController", templateName = "./base", view = new View()){
        this.controllerName = controllerName
        this.templateName = templateName
        this.view = view
    }

    setView(view){
        this.view = view
    }
    getView(){
        return this.getView()
    }
}

class ControllerDecorator extends Controller{
    getView(){
        return super.getView
    }
}