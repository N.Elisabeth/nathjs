class Router {

    constructor(){
        this.controllers = []
        this.routes = [
            {
                "path": "#/",
                "controller": "ControllerA"
            },
            {
                "path": "#/B",
                "controller": "ControllerB"
            }
        ]
    }

    setControllersList(controllers){
        this.controllers = controllers
    }

    chooseController(path){
        var controllerPath = this.routes.find(route => route.path === path)

        if(controllerPath != null){
            console.log(controllerPath.controller)
            console.log(this.controllers)
            console.log(this.controllers[controllerPath.controller])
            return this.controllers[controllerPath.controller]
        }
        console.log(this.routes)
        return this.controllers[this.routes.find(route => route.path === "#/").controller]
    }
}