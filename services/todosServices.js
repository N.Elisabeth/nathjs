class TodosService{
    
    constructor(){

        this.todosList = localStorage.getItem("todos") != null ? JSON.parse(localStorage.getItem("todos")) : this.todosList = [ {id: 1, text: "essaie 1", toggle: false}, {id: 2, text: "essaie 2", toggle: false} ]
    }

    getTodosList(){
        return this.todosList
    }

    addTodo(text){
        const todo = {
            id: this.todosList.length > 0 ? this.todosList.length + 1 : 1,
            text: text != "" ? text : "todo",
            toggle: false
        }
        this.todosList.push(todo)
        localStorage.setItem("todos", JSON.stringify(this.todosList))
        return todo
    }

    toggleChange(index, state){
        this.todosList[index].toggle = state
        localStorage.setItem("todos", JSON.stringify(this.todosList))
    }

    removeTodo(index){
        this.todosList = this.todosList.filter(todo => todo.id !== this.todosList[index].id);
        localStorage.setItem("todos", JSON.stringify(this.todosList))
    }
}

const todosService = new TodosService()